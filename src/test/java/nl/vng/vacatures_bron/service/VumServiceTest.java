package nl.vng.vacatures_bron.service;

import nl.vng.vacatures_bron.config.ConfigProperties;
import nl.vng.vacatures_bron.dto.VacatureMatchesRequest;
import nl.vng.vacatures_bron.dto.VacatureMatchingProfielen;
import nl.vng.vacatures_bron.entity.MPVacatureMatch;
import nl.vng.vacatures_bron.entity.Vacature;
import nl.vng.vacatures_bron.mapper.SimpleMapper;
import nl.vng.vacatures_bron.repository.VacatureRepository;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class VumServiceTest {

    private static final int MAX_AMOUNT = 3;
    private static final String ID1 = "ID1";
    private static final String OIN = "123456789";

    @Mock
    private VacatureRepository repository;

    @Mock
    private SimpleMapper mapper;

    private ConfigProperties properties;

    @InjectMocks
    private VumService vumService;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private VacatureMatchesRequest request;
    @Mock
    private MPVacatureMatch mpVacatureMatch;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Vacature vacature;

    @BeforeEach
    void setUp() {
        properties = mock(ConfigProperties.class);
        when(properties.getMaxAmountResponse()).thenReturn(MAX_AMOUNT);

        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldFindByIdSuccess() {
        vacature.setOin(OIN);

        when(repository.findByIdVacatureAndOin(ID1, OIN)).thenReturn(Optional.of(vacature));

        vumService.findById(ID1, OIN);

        verify(repository).findByIdVacatureAndOin(ID1, OIN);
    }

    @Test
    void shouldFindByIdFailure() {
        when(repository.findByIdVacatureAndOin(ID1, OIN)).thenReturn(Optional.empty());

        vumService.findById(ID1, OIN);

        verify(repository).findByIdVacatureAndOin(ID1, OIN);
    }

    @Test
    void shouldMatchEmpty() {
        when(repository.findAll(ArgumentMatchers.<Specification<Vacature>>any())).thenReturn(List.of());

        ImmutablePair<Boolean, VacatureMatchingProfielen> result = vumService.match(request, OIN);

        assertThat(result.getLeft()).isFalse();
        Assertions.assertThat(result.getRight().getMatches()).isEmpty();

    }

    @Test
    void shouldMatchOne() {
        when(repository.findAll(ArgumentMatchers.<Specification<Vacature>>any())).thenReturn(List.of(vacature));
        when(mapper.vacatureToMPVacatureMatch(any())).thenReturn(mpVacatureMatch);

        ImmutablePair<Boolean, VacatureMatchingProfielen> result = vumService.match(request, OIN);
        assertThat(result.getLeft()).isFalse();
        Assertions.assertThat(result.getRight().getMatches()).containsExactly(mpVacatureMatch);

    }

    @Test
    void shouldMatchMaxAmount() {

        List<Vacature> vacatureList100 = new ArrayList<>(Collections.nCopies(MAX_AMOUNT, vacature));
        List<MPVacatureMatch> mPvacatureList100 = new ArrayList<>(Collections.nCopies(MAX_AMOUNT, mpVacatureMatch));


        when(repository.findAll(ArgumentMatchers.<Specification<Vacature>>any())).thenReturn(vacatureList100);
        when(mapper.vacatureToMPVacatureMatch(any())).thenReturn(mpVacatureMatch);

        ImmutablePair<Boolean, VacatureMatchingProfielen> result = vumService.match(request, OIN);
        assertThat(result.getLeft()).isTrue();
        Assertions.assertThat(result.getRight().getMatches()).hasSameElementsAs(mPvacatureList100);
    }

    @Test
    void shouldMatchMaxAmountMinusOne() {

        List<Vacature> vacatureList100 = new ArrayList<>(Collections.nCopies(MAX_AMOUNT - 1, vacature));
        List<MPVacatureMatch> mPvacatureList100 = new ArrayList<>(Collections.nCopies(MAX_AMOUNT - 1, mpVacatureMatch));


        when(repository.findAll(ArgumentMatchers.<Specification<Vacature>>any())).thenReturn(vacatureList100);
        when(mapper.vacatureToMPVacatureMatch(any())).thenReturn(mpVacatureMatch);

        ImmutablePair<Boolean, VacatureMatchingProfielen> result = vumService.match(request, OIN);
        assertThat(result.getLeft()).isFalse();
        Assertions.assertThat(result.getRight().getMatches()).hasSameElementsAs(mPvacatureList100);
    }

    @Test
    void shouldShuffle() {

        Vacature vacature1 = mock(Vacature.class);
        Vacature vacature2 = mock(Vacature.class);

        List<Vacature> werkzoekenden = Arrays.asList(vacature, vacature1, vacature2);
        List<Vacature> result = vumService.pickRandom(werkzoekenden, 2);
        assertEquals(2, result.size());
    }
}