package nl.vng.vacatures_bron.repository;

import nl.vng.vacatures_bron.config.ConfigProperties;
import nl.vng.vacatures_bron.dto.VacatureMatchesRequest;
import nl.vng.vacatures_bron.dto.VacatureMatchingProfielen;
import nl.vng.vacatures_bron.entity.MPVacature;
import nl.vng.vacatures_bron.entity.Vacature;
import nl.vng.vacatures_bron.mapper.SimpleMapper;
import nl.vng.vacatures_bron.service.ElkService;
import nl.vng.vacatures_bron.service.VumService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import(ConfigProperties.class)
@ComponentScan(basePackageClasses = {VumService.class, SimpleMapper.class} , excludeFilters={
        @ComponentScan.Filter(type= FilterType.ASSIGNABLE_TYPE, value= ElkService.class)})
@ActiveProfiles("test")
class SpecificationOinTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private VumService service;


    private static final String OIN = "123456789";
    private static final String differentOIN = "987654321";
    private static VacatureMatchesRequest vacatureRequest;
    private static Vacature vacature;

    @BeforeEach
    void setUp() {
        vacature = new Vacature();
        MPVacature mpVacature = new MPVacature();
        vacatureRequest = new VacatureMatchesRequest(mpVacature);

    }



    /**
     * Util method to persist and assert empty result.
     */
    private void assertEmpty(String oin) {
        entityManager.persistAndFlush(vacature);
        ImmutablePair<Boolean, VacatureMatchingProfielen> result = service.match(vacatureRequest, oin);
        Assertions.assertThat(result.getRight().getMatches()).isEmpty();
        entityManager.flush();
    }

    /**
     * Util method to persist and assert a match.
     */
    private void assertFound(String oin) {
        entityManager.persistAndFlush(vacature);
        ImmutablePair<Boolean, VacatureMatchingProfielen> result = service.match(vacatureRequest, oin);
        Assertions.assertThat(result.getRight().getMatches()).hasSize(1);
    }

    @Test
    void equalOinMatches() {
        vacature.setOin(OIN);
        assertFound(OIN);
    }

    @Test
    void differentOinDoesNotMatch() {
        vacature.setOin(OIN);
        assertEmpty(differentOIN);
    }
}
