package nl.vng.vacatures_bron.controller;

import nl.vng.vacatures_bron.dto.VacatureMatchingProfielen;
import nl.vng.vacatures_bron.config.ConfigProperties;
import nl.vng.vacatures_bron.dto.VacatureMatchesRequest;
import nl.vng.vacatures_bron.entity.Vacature;
import nl.vng.vacatures_bron.exception.MismatchCertWithOinVumProviderException;
import nl.vng.vacatures_bron.exception.VacatureNotFoundException;
import nl.vng.vacatures_bron.service.ElkService;
import nl.vng.vacatures_bron.service.VumService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the VumController
 */
@ExtendWith(MockitoExtension.class)
class VumControllerTest {

    @Mock
    private VumService vumService;

    @Mock
    private ElkService elkService;
    
    @Mock
    private ConfigProperties properties;

    @InjectMocks
    private VumController vumController;

    private final String idVacature = "idVacature";
    private final String xVUMBerichtVersie = "xVUMBerichtVersie";
    private final String xVUMFromParty = "xVUMFromParty";
    private final String xVUMToParty = "xVUMToParty";
    private Boolean xVUMSUWIparty;
    private final String xVUMViaParty = "xVUMViaParty";
    private final String xForwardedFor = "subjectNumber=1234567890123456789";
 
    @Mock
    private VacatureMatchesRequest matchesRequest;
    @Mock
    private HttpServletRequest request;

    @Test
    void shouldGetVacatureProfiel() {

        Optional<Vacature> VacatureOptional = Optional.of(new Vacature());
        when(properties.getVumProviderOin()).thenReturn("00000000000001234567890123456789");
        
        when(vumService.findById(idVacature, xVUMToParty)).thenReturn(VacatureOptional);

        ResponseEntity<Vacature> vacature = vumController.getVacature(idVacature, xVUMBerichtVersie, xVUMFromParty, xVUMToParty, xVUMSUWIparty, xForwardedFor,  xVUMViaParty);

        String requestSignature = "GET /vacatures/{idVacature}";
        String zoekopdracht_voor_matchingProfielen_Vacature = "Vraag volledige vacature op bij opgegeven idVacature";
        String requestPath = "/vacatures/" + idVacature;

        verify(elkService).handleRequest(VacatureOptional.get(), xVUMToParty, xVUMFromParty, requestSignature, zoekopdracht_voor_matchingProfielen_Vacature, requestPath);
        verify(vumService).findById(idVacature, xVUMToParty);

        assertNotNull(vacature);
        assertEquals(VacatureOptional.get(), vacature.getBody());
    }

    @Test
    void shouldNotGetVacatureProfiel() {

        Optional<Vacature> VacatureOptional = Optional.empty();
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");

        when(vumService.findById(idVacature, xVUMToParty)).thenReturn(VacatureOptional);

        Assertions.assertThrows(VacatureNotFoundException.class, () -> vumController.getVacature(idVacature, xVUMBerichtVersie, xVUMFromParty, xVUMToParty, xVUMSUWIparty, xForwardedFor, xVUMViaParty), "VacatureNotFoundException is verwacht");
    }
    
    @Test
    void shouldNotGetVacatureProfiel_MismatchCertWithOinVumProvider() {

        Optional<Vacature> VacatureOptional = Optional.empty();
        when(properties.getVumProviderOin()).thenReturn("01234567890123450000");

        Assertions.assertThrows(MismatchCertWithOinVumProviderException.class, () -> vumController.getVacature(idVacature, xVUMBerichtVersie, xVUMFromParty, xVUMToParty, xVUMSUWIparty, xForwardedFor, xVUMViaParty), "MismatchCertWithOinVumProviderException is verwacht");
    }

    @Test
    void shouldMatchVacatureProfielen() {

        ImmutablePair<Boolean, VacatureMatchingProfielen> matches = new ImmutablePair<>(true, new VacatureMatchingProfielen(Collections.emptyList()));
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        when(vumService.match(matchesRequest, xVUMToParty)).thenReturn(matches);

        vumController.matchesVacatures(xVUMBerichtVersie, xVUMFromParty, xVUMToParty, xVUMSUWIparty, matchesRequest, xForwardedFor, xVUMViaParty);

        String requestSignature = "POST /vacatures/matches";
        String zoekopdracht_voor_matchingProfielen_vacature = "Zoekopdracht voor Matching Vacature";
        String requestPath = "/vacatures/matches";
        
        verify(elkService).handleRequest(matchesRequest, xVUMToParty, xVUMFromParty, requestSignature, zoekopdracht_voor_matchingProfielen_vacature,
                requestPath);
        verify(vumService).match(matchesRequest, xVUMToParty);
    }
    
    @Test
    void matchVacatureProfielen_MismatchCertWithOinVumProvider() {
    	
    	ImmutablePair<Boolean, VacatureMatchingProfielen> matches = new ImmutablePair<>(true, new VacatureMatchingProfielen(Collections.emptyList()));
        Optional<Vacature> VacatureOptional = Optional.empty();
        when(properties.getVumProviderOin()).thenReturn("01234567890123450000");
        
        Assertions.assertThrows(MismatchCertWithOinVumProviderException.class, () -> vumController.matchesVacatures(xVUMBerichtVersie, xVUMFromParty, xVUMToParty, xVUMSUWIparty, matchesRequest, xForwardedFor, xVUMViaParty), "MismatchCertWithOinVumProviderException is verwacht");
    }
    
}