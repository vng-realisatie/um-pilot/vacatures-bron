package nl.vng.vacatures_bron.controller;

import nl.vng.vacatures_bron.entity.Vacature;
import nl.vng.vacatures_bron.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.vacatures_bron.service.ElkService;
import nl.vng.vacatures_bron.service.GemeenteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the GemeenteController
 */
@ExtendWith(MockitoExtension.class)
class GemeenteControllerTest {

    public static final String OIN = "OIN";
    @Mock
    private ElkService elkService;

    @Mock
    private GemeenteService gemeenteService;

    @Mock
    private SecurityContext securityContext;

    private JwtAuthenticationToken authentication;

    @InjectMocks
    private GemeenteController gemeenteController;

    @BeforeEach
    private void setUp() {
        authentication = new JwtAuthenticationToken(Jwt
                .withTokenValue("oin")
                .claim("oin", OIN)
                .header("", "")
                .build());
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void shouldAddWerkzoekendeList() {

        List<Vacature> werkzoekenden = Arrays.asList(new Vacature());
        gemeenteController.createVacatureList(werkzoekenden, OIN);

        String requestSignature = "POST /vacature/lijst/{oin}";
        String requestDescription = "Endpoint to create several Vacature from a list";
        String requestPath = "/vacature/lijst/OIN";

        verify(elkService).handleRequest(werkzoekenden, OIN, OIN, requestSignature, requestDescription, requestPath);
        verify(gemeenteService).saveAll(werkzoekenden, OIN);
    }

    @Test
    void shouldFindAll() {

        List<Vacature> werkzoekenden = Arrays.asList(new Vacature());
        when(gemeenteService.findAll(OIN)).thenReturn(werkzoekenden);

        List<Vacature> all = gemeenteController.getAll(OIN);

        String requestSignature = "GET /vacature/lijst/{oin}";
        String requestDescription = "Endpoint to retrieve Vacature from the DB";
        String requestPath = "/vacature/lijst/OIN";

        verify(gemeenteService).findAll(OIN);
        verify(elkService).handleRequest(werkzoekenden, OIN, OIN, requestSignature, requestDescription, requestPath);

        assertEquals(werkzoekenden, all);
    }

    @Test()
    void shouldFailBuildingOinUserGetAll() {

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.getAll("WRONG"), "WrongOinException is verwacht");
    }

    @Test()
    void shouldFailBuildingOinUserCreate() {

        List<Vacature> werkzoekenden = Arrays.asList(new Vacature());

        Assertions.assertThrows(MismatchRequestWithOinBemiddelaarException.class, () -> gemeenteController.createVacatureList(werkzoekenden, "WRONG"), "WrongOinException is verwacht");
    }
}