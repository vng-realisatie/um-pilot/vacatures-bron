package nl.vng.vacatures_bron.controller;

import nl.vng.vacatures_bron.config.ConfigProperties;
import nl.vng.vacatures_bron.dto.InlineResponse200;
import nl.vng.vacatures_bron.dto.VacatureMatchesRequest;
import nl.vng.vacatures_bron.dto.VacatureMatchingProfielen;
import nl.vng.vacatures_bron.entity.MPVacature;
import nl.vng.vacatures_bron.entity.MPVacatureMatch;
import nl.vng.vacatures_bron.entity.Vacature;
import nl.vng.vacatures_bron.service.ElkService;
import nl.vng.vacatures_bron.service.VumService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = VumController.class)
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
class VumControllerITCase {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private VumService vumService;

    @MockBean
    private ElkService elkService;
    
    @MockBean
    private ConfigProperties properties;

    private static final String ID = "exampleid";
    private static final String OIN = "00000001821002198000";
    private static final String PATH = "./src/test/resources/";

    private static Vacature vacature;
    private static MPVacatureMatch mpVacatureMatch;


    @BeforeEach
    void setUp() {
        try {
            vacature = objectMapper.readValue(new File(PATH, "vacature.json"), Vacature.class);
            mpVacatureMatch = objectMapper.readValue(new File(PATH, "mp-vacature.json"), MPVacatureMatch.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    @Test
    void getVacatureFound() throws Exception {
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        when(vumService.findById(ID, TestRequestFactory.X_VUM_TO_PARTY_OIN)).thenReturn(Optional.ofNullable(vacature));

        mockMvc.perform(TestRequestFactory.getWithHeaders("/vacatures/" + ID)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(vacature)));
    }

    @Test
    void getVacatureNotFound() throws Exception {
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        when(vumService.findById(any(), any())).thenReturn(Optional.empty());

        mockMvc.perform(TestRequestFactory.getWithHeaders("/vacatures/" + ID)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"code\":\"400.01\",\"message\":\"Ongeldige aanroep\",\"details\":\"ID niet gevonden\"}"));
    }

    @Test
    void matchVacatureFound() throws Exception {

        VacatureMatchesRequest request = new VacatureMatchesRequest(new MPVacature());
        VacatureMatchingProfielen response = new VacatureMatchingProfielen(List.of(new MPVacatureMatch[]{mpVacatureMatch}));
        
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        when(vumService.match(any(), eq(OIN))).thenReturn(ImmutablePair.of(false, response));

        InlineResponse200 inlineResponse = new InlineResponse200(false, response);

        mockMvc.perform(TestRequestFactory.postWithHeaders("/vacatures/matches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(inlineResponse)));

    }

    @Test
    void matchVacatureEmpty() throws Exception {

        VacatureMatchesRequest request = new VacatureMatchesRequest(new MPVacature());
        VacatureMatchingProfielen response = new VacatureMatchingProfielen(List.of(new MPVacatureMatch[]{}));
        
        when(properties.getVumProviderOin()).thenReturn("01234567890123456789");
        when(vumService.match(any(), eq(OIN))).thenReturn(ImmutablePair.of(false, response));

        InlineResponse200 inlineResponse = new InlineResponse200(false, response);

        mockMvc.perform(TestRequestFactory.postWithHeaders("/vacatures/matches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(inlineResponse)));

    }
}