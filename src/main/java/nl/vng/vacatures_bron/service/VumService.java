package nl.vng.vacatures_bron.service;

import nl.vng.vacatures_bron.config.ConfigProperties;
import nl.vng.vacatures_bron.dto.VacatureMatchesRequest;
import nl.vng.vacatures_bron.dto.VacatureMatchingProfielen;
import nl.vng.vacatures_bron.entity.MPVacatureMatch;
import nl.vng.vacatures_bron.entity.Vacature;
import nl.vng.vacatures_bron.mapper.SimpleMapper;
import nl.vng.vacatures_bron.repository.VacatureRepository;
import nl.vng.vacatures_bron.repository.VacatureSpecifications;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VumService {

    private final VacatureRepository repository;

    private final SimpleMapper mapper;

    private final int maxAmount;

    public VumService(final VacatureRepository vacatureRepository, final SimpleMapper simpleMapper, final ConfigProperties configProperties) {
        this.repository = vacatureRepository;
        this.mapper = simpleMapper;
        maxAmount = configProperties.getMaxAmountResponse();

    }

    /**
     * Finds a specific Vacature for an OIN
     * @param idVacature the id of the Vacature
     * @param oin identifier of municipality of Vacature
     * @return the Vacature identified by Id
     */
    public Optional<Vacature> findById(final String idVacature, final String oin) {
        return repository.findByIdVacatureAndOin(idVacature, oin);
    }

    public ImmutablePair<Boolean, VacatureMatchingProfielen> match(final VacatureMatchesRequest matchesRequest, final String toOin) {

        // Get the specifications based on the matchesRequest.
        Specification<Vacature> vacatureSpecifications = VacatureSpecifications
                .createVacatureSpecification(matchesRequest.getVraagObject(), toOin);
        // Search the DB with these specifications.
        List<Vacature> vacatures = repository.findAll(vacatureSpecifications);
        // Shuffle and limit vacatures.
        List<Vacature> limitedVacatures = pickRandom(vacatures, maxAmount);
        // use mapper to map Vacature to MPVacatureMatch
        List<MPVacatureMatch> mpVacatureMatches = limitedVacatures
                .stream()
                .map(mapper::vacatureToMPVacatureMatch)
                .collect(Collectors.toList());
        return new ImmutablePair<>(vacatures.size() >= maxAmount, new VacatureMatchingProfielen(mpVacatureMatches));


    }

    /**
     * Util function to randomly pick max amount of vacature coming from the DB.
     *
     * @param vacatureList List to be limited.
     * @param max    Max amount.
     * @return Shuffled and limited list of vacature.
     */
    public List<Vacature> pickRandom(final List<Vacature> vacatureList, final int max) {
        // Already less than max amount.
        if (vacatureList.size() <= max) {
            return vacatureList;
        }
        //Shuffle in O(n).
        Collections.shuffle(vacatureList);
        // Pick max amount from new list.
        return vacatureList.stream().limit(max).collect(Collectors.toList());

    }
}
