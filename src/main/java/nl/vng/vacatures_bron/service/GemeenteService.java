package nl.vng.vacatures_bron.service;

import nl.vng.vacatures_bron.entity.Vacature;
import nl.vng.vacatures_bron.repository.VacatureRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GemeenteService {

    private final VacatureRepository repository;

    public GemeenteService(final VacatureRepository vacatureRepository) {
        this.repository = vacatureRepository;
    }

    /**
     * Save all Vacature from the list into the DB.
     *
     * @param vacatures list of Vacature to be saved in the DB.
     * @param oin       identifier of municipality of Vacature.
     * @return List of Vacature entities after being saved in the DB.
     */
    public List<Vacature> saveAll(final List<Vacature> vacatures, final String oin) {
        // first delete all vacatures
        repository.deleteByOin(oin);
        vacatures.forEach(x -> {
            //Add OIN to each vacature before saving to DB
            x.setOin(oin);
        });
        return repository.saveAll(vacatures);
    }

    /**
     * Provides a list of all Vacatures for a specific OIN
     *
     * @param oin identifier of municipality of Vacature.
     * @return List of Vacature entities
     */
    public List<Vacature> findAll(final String oin) {
        return repository.findAllByOin(oin);
    }
}
