package nl.vng.vacatures_bron;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocVngApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocVngApplication.class, args);
    }

}
