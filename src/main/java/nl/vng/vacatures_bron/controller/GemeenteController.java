package nl.vng.vacatures_bron.controller;

import nl.vng.vacatures_bron.entity.Vacature;
import nl.vng.vacatures_bron.exception.MismatchRequestWithOinBemiddelaarException;
import nl.vng.vacatures_bron.service.ElkService;
import nl.vng.vacatures_bron.service.GemeenteService;
import nl.vng.vacatures_bron.util.Error;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@Tag(name = "vacatures")
@CrossOrigin()
@Slf4j
public class GemeenteController {

    private final GemeenteService service;

    private final ElkService elkService;

    @Autowired
    public GemeenteController(final GemeenteService gemeenteService, final ElkService elk) {
        this.service = gemeenteService;
        this.elkService = elk;
    }

    /**
     * POST /vacature/lijst/{oin} : Endpoint to create several Vacature from a list.
     *
     * @param vacatures list of Vacature
     * @param oin       identifier of municipality
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Internal Server Error (status code 500)
     */
    @Operation(summary = "Post vacatures", description = "Post vacatures to a given OIN", tags = {})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Vacature.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "401", description = "not authenticated", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "403", description = "Geen authorizatie", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = Error.class)))})
    @PostMapping(
            value = "vacature/lijst/{oin}",
            produces = {"application/json"}
    )
    public List<Vacature> createVacatureList(@Valid @RequestBody List<Vacature> vacatures, @PathVariable String oin) {
        log.info("Upload van vacatures door OIN: " + oin);
        String oinUser = getOinUser();
        if (oinUser.equals(oin)) {
            log.info("Succesvolle upload van vacatures door OIN: " + oinUser);
            elkService.handleRequest(
                    vacatures,
                    oin,
                    oinUser,
                    "POST /vacature/lijst/{oin}",
                    "Endpoint to create several Vacature from a list",
                    "/vacature/lijst/" + oin
            );
            return service.saveAll(vacatures, oin);
        } else {
            log.info("Mislukte upload van vacatures door OIN: " + oinUser + "Er is geprobeerd te uploaden voor OIN:" + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    oinUser,
                    "POST /vacature/lijst/{oin}",
                    "Endpoint to create several Vacature from a list",
                    "/vacature/lijst/" + oin
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }
    }

    /**
     * GET /vacature/lijst/{oin} : Endpoint to retrieve Vacature from the DB.
     *
     * @param oin identifier of municipality
     * @return OK (status code 200)
     * or Bad request (status code 400)
     * or not authorized (status code 401)
     * or Forbidden (status code 403)
     * or Internal Server Error (status code 500)
     */
    @Operation(summary = "Get vacatures", description = "Get vacatures from DB", tags = {})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(array = @ArraySchema(schema = @Schema(implementation = Vacature.class)))),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "401", description = "not authenticated", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "403", description = "Geen authorizatie", content = @Content(schema = @Schema(implementation = Error.class))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content(schema = @Schema(implementation = Error.class)))})
    @GetMapping(value = "vacature/lijst/{oin}")
    public List<Vacature> getAll(@PathVariable String oin) {
        String oinUser = getOinUser();
        if (oinUser.equals(oin)) {
            List<Vacature> vacatures = service.findAll(oin);
            elkService.handleRequest(
                    vacatures,
                    oin,
                    oinUser,
                    "GET /vacature/lijst/{oin}",
                    "Endpoint to retrieve Vacature from the DB",
                    "/vacature/lijst/" + oin
            );
            return vacatures;
        } else {
            log.info("Mislukte GET van vacatures door OIN: " + oinUser + "Er is geprobeerd te GET voor OIN:" + oin);
            elkService.handleRequest(
                    new MismatchRequestWithOinBemiddelaarException(),
                    oin,
                    oinUser,
                    "GET /vacature/lijst/{oin}",
                    "Endpoint to retrieve Vacature from the DB",
                    "/vacature/lijst/" + oin
            );
            throw new MismatchRequestWithOinBemiddelaarException();
        }

    }

    /**
     * Utility method to extract OIN from the jwt token given by the user.
     *
     * @return OIN of the user or empty string if no OIN given.
     */
    private String getOinUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String oinUser = ((JwtAuthenticationToken) authentication).getToken().getClaim("oin");
        
        return oinUser == null ?  "" : oinUser;
    }
}
