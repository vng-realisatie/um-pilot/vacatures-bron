package nl.vng.vacatures_bron.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VacatureCompositeId implements Serializable {

    private String idVacature;
    private String oin;

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false;}
        VacatureCompositeId vacatureCompositeId = (VacatureCompositeId) o;
        return idVacature.equals(vacatureCompositeId.idVacature) &&
                oin.equals(vacatureCompositeId.oin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idVacature, oin);
    }
}
