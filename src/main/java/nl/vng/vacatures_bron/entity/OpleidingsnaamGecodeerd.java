package nl.vng.vacatures_bron.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.Valid;


/**
 * OpleidingsnaamGecodeerd
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class OpleidingsnaamGecodeerd extends Opleidingsnaam implements MPOpleidingsnaam {

    @Valid
    @Embedded
    private OpleidingsnaamGecodeerdImpl opleidingsnaamGecodeerd;

}
