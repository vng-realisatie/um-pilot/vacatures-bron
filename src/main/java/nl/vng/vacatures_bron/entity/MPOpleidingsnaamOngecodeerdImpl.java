package nl.vng.vacatures_bron.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MPOpleidingsnaamOngecodeerdImpl {
    @Size(max = 120)
    private String naamOpleidingOngecodeerd;
}
