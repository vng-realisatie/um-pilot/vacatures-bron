package nl.vng.vacatures_bron.mapper;

import nl.vng.vacatures_bron.entity.Adres;
import nl.vng.vacatures_bron.entity.AdresBuitenland;
import nl.vng.vacatures_bron.entity.AdresNederland;
import nl.vng.vacatures_bron.entity.MPAdresBuitenland;
import nl.vng.vacatures_bron.entity.MPAdresBuitenlandImpl;
import nl.vng.vacatures_bron.entity.MPOpleidingsnaam;
import nl.vng.vacatures_bron.entity.MPOpleidingsnaamOngecodeerd;
import nl.vng.vacatures_bron.entity.MPVacatureMatch;
import nl.vng.vacatures_bron.entity.Opleidingsnaam;
import nl.vng.vacatures_bron.entity.OpleidingsnaamGecodeerd;
import nl.vng.vacatures_bron.entity.OpleidingsnaamOngecodeerd;
import nl.vng.vacatures_bron.entity.Vacature;
import org.mapstruct.Mapper;

/**
 * Interface from which Mapstruct generates a mapper from Vacature to MPVacatureMatch.
 */
@Mapper(
        componentModel = "spring"
)
public interface SimpleMapper {

    MPVacatureMatch vacatureToMPVacatureMatch(Vacature vacature);

    default MPOpleidingsnaam map(final Opleidingsnaam value) {
        if (value instanceof OpleidingsnaamGecodeerd) {
            return (OpleidingsnaamGecodeerd) value;
        } else {
            return opleidingsnaamOngecodeerdToMPOpleidingsnaamOngecodeerd((OpleidingsnaamOngecodeerd) value);
        }
    }

    MPOpleidingsnaamOngecodeerd opleidingsnaamOngecodeerdToMPOpleidingsnaamOngecodeerd(OpleidingsnaamOngecodeerd value);


    default MPAdresBuitenland adresToMPAdresBuitenland(final Adres value){
        if(value instanceof AdresNederland){
            return adresNederlandToMPAdresBuitenland((AdresNederland) value);
        }
        else{
            return adresBuitenlandToMPAdresBuitenland((AdresBuitenland) value);
        }
    }

    MPAdresBuitenland adresBuitenlandToMPAdresBuitenland(AdresBuitenland value);


    default MPAdresBuitenland adresNederlandToMPAdresBuitenland(final AdresNederland value){
        MPAdresBuitenland result = new MPAdresBuitenland();
        MPAdresBuitenlandImpl impl = new MPAdresBuitenlandImpl();
        impl.setLandencodeIso("NL");
        result.setAdresBuitenland(impl);
        return result;
    }
}
