package nl.vng.vacatures_bron.repository;

import nl.vng.vacatures_bron.entity.Vacature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface VacatureRepository extends JpaRepository<Vacature, String>, JpaSpecificationExecutor<Vacature> {

    Optional<Vacature> findByIdVacatureAndOin(String idVacature, String oin);

    List<Vacature> findAllByOin(String oin);

    @Transactional
    List<Vacature> deleteByOin(String oin);
}
