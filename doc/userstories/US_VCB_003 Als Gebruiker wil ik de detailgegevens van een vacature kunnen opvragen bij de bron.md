# Feature : US_VCB_003 Als Gebruiker wil ik de detailgegevens van een vacature kunnen opvragen bij de bron

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2022 | Initiele opzet                        |

**Als** Gebruiker  
**wil ik** de detailgegevens van een vacature kunnen opvragen bij de bron
**zodat** de gegevens van een vacature geraadpleegd kunnen worden

### Functioneel
Deze userstory ondersteunt de mogelijkheid een vacature op te kunnen vragen

### Technische Documentatie

TODO Van deze operatie is nog geen yml beschikbaar  
End point : /vacatures/{idVacature}

### Acceptatiecriteria

*Feature: opvragen bestaande vacature*
**Gegeven** de Gebruiker is geautoriseerd  
**Wanneer** een vacature wordt opgevraagd  
**En** de vacature is aanwezig in de bron  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** wordt de vacature in de response geleverd

*Scenario: opvragen niet bestaand werkzoekende profiel*  
**Gegeven** de Gebruiker is geautoriseerd  
**Wanneer** een vacature wordt opgevraagd
**En** de vacature is niet aanwezig in de bron  
**Dan** retourneert de applicatie een http status 400 (Bad Request)  
**En** wordt er geen response geretourneerd

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 500 (Internal Server Error)  
