# Feature : US_VCB_004 Als Bemiddelaar wil ik vacatures vinden die aan de matchingscriteria voldoen

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2022 | Initiele opzet                        |

**Als** Bemiddelaar
**wil ik** vacatures vinden die aan de matchingscriteria voldoen
**zodat** de gegevens van de vacatures geraadpleegd kunnen worden

### Functioneel
De bemiddeling naar werk kan zowel plaats vinden op profielen als op vacatures. 
In deze userstory ligt het zoeken van vacatures obv criteria vast. 
Met andere woorden er worden vacatures gezocht die voldoen aan bepaalde eisen.
De criteria waarop gezocht kan worden zijn uitgebreid en worden hier verder niet in detail beschreven.
Voor een overzicht van criteria wordt verwezen naar de yml

### Technische Documentatie
Van deze operatie is geen yml beschikbaar  
End point : /vacatures/matches

### Acceptatiecriteria

*Feature: opvragen matchende vacatures*
**Gegeven** de Gebruiker is geautoriseerd  
**Wanneer** een er gezocht wordt op vacatures  
**En** er bestaan vacatures in de bron  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**EN** worden er willekeurig maximaal x willekeurige vacatures in de response geleverd

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 500 (Internal Server Error)  
