# Feature : US_VCB_002 Als Vacatureverantwoordelijke wil ik vacatures kunnen opvragen bij de bron

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | december 2022 | Initiele opzet                        |


**Als** Vacatureverantwoordelijke
**wil ik** vacatures kunnen opvragen bij de bron
**zodat** de gegevens van een vacature geraadpleegd kunnen worden

### Functioneel
Deze userstory ondersteunt de mogelijkheid vacatures van een OIN op te kunnen vragen

### Technische Documentatie
End point : vacature/lijst/{oin}

### Acceptatiecriteria

*Feature: opvragen bestaande vacatures*
**Gegeven** de Gebruiker is geautoriseerd  
**Wanneer** de vacatures van een OIN worden opgevraagd  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** wordt de vacatures van dat OIN in de response geleverd

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)  

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 500 (Internal Server Error)  
