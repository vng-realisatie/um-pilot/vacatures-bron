# Feature : US_VCB_001 Als Vacatureverantwoordelijke wil ik vacatures kunnen aanbieden aan de bron

versie 0.10

_Versionering_

| versie | datum         | opmerking                 |
|--------|---------------|---------------------------|
| 0.10   | januari 2023  | Initiele opzet            |

**Als** Vacatureverantwoordelijke  
**Wil ik** vacatures kunnen aanbieden aan de bron  
**Zodat ik** deze vacatures kan gebruiken voor matchingsdoeleinden

### Functioneel
Dit is een ondersteunende User Story die beschrijft waaraan de API dient te voldoen.

Vacature Ids worden van buiten af geleverd. 
UM heeft daar geen controle over. 
Tussen vacature aanbieders is het niet uitgesloten dat deze identieke vacaturenummers gebruiken. 
Hiertoe dienen binnen een OIN vacatureIds (net als werkzoekendeIds) uniek te zijn.

### Technische Documentatie

TODO

In de implementatie worden de volgende response codes genoemd
"200", description = "OK"
"400", description = "Bad request"
"401", description = "not authorized"
"429", description = "Too Many Requests, limiet is overschreden voor deze uitvraag"
"500", description = "Internal Server Error"
"503", description = "Service Unavailable"


#### Postconditie
Na een bewerking is altijd de log bijgewerkt

### Acceptatiecriteria

*Feature: Toevoegen vacatures*  
**Gegeven** de Client is geauthoriseerd  
**En** er zijn voor het OIN geen vacatures aanwezig in de bron  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** zijn de vacatures opgenomen in de bron   

*Scenario: Vervangen vacatures*  
**Gegeven** de Client is geauthoriseerd  
**En** er zijn voor het OIN vacatures aanwezig in de bron  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 200 (Response OK)  
**En** zijn de bestaande vacatures verwijderd uit de bron   
**En** zijn de nieuwe vacatures opgenomen in de bron   

*Feature: Toevoegen niet valide vacatures*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een valide JSON die niet voldoet aan de Business Rules aan wordt geboden  
**Dan** retourneert de applicatie een http status 400 (Bad Request)  
**En** is de vacature niet opgenomen in de bron   

*Scenario: niet geautoriseerd*  
**Gegeven** de Client is niet geauthoriseerd  
**Wanneer** een valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 401 (not authorized)  
**En** zijn de vacatures niet opgenomen in de bron  

*Scenario: verwerking niet valide JSON*  
**Gegeven** de Client is geauthoriseerd  
**Wanneer** een niet valide JSON aan wordt geboden  
**Dan** retourneert de applicatie een http status 500 (Internal Server Error)  
**En** zijn de vacatures niet opgenomen in de bron

*Scenario : registreren vacature bij OIN*  
**Gegeven** een gebruiker voert vacatures op   
**En** het vacatureId komt niet voor in de database
**Wanneer** de JSON enkel unieke vacatureIds bevat
**En** de JSON voldoet aan alle overige eisen  
**Dan** worden de gegevens uit de JSON in de database opgeslagen

*Scenario : registreren vacature bij verschillende OINs*  
**Gegeven** een gebruiker voert werkzoekendeprofielen op   
**En** het vacatureId komt reeds voor in de database bij een ander OIN
**Wanneer** de JSON enkel unieke vacatureIds bevat
**En** de JSON voldoet aan alle overige eisen  
**Dan** worden de gegevens uit de JSON in de database opgeslagen

*Scenario : registreren dubbele vacature bij hetzelfde OIN*  
**Gegeven** een gebruiker voert vacatures op   
**Wanneer** de JSON een dubbel vacatureId bevat
**En** de JSON voldoet aan alle overige eisen  
**Dan** worden de gegevens uit de JSON niet in de database opgeslagen

*Scenario : registreren vacature zonder Id bij hetzelfde OIN*  
**Gegeven** een gebruiker voert vacatures op   
**Wanneer** de JSON een entiteit met een leeg vacatureId bevat
**En** de JSON voldoet aan alle overige eisen  
**Dan** worden de gegevens uit de JSON niet in de database opgeslagen
